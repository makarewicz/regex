module RegExtra where
import Mon
import Reg
import Data.List

data AB = A | B deriving(Eq,Ord,Show)

infix 4 ===
class Equiv a where
  (===) :: a -> a -> Bool

instance (Eq c) => Equiv (Reg c) where
   r1 === r2 = (fix_assoc $ simpl r1) == (fix_assoc $ simpl r2)

instance Mon (Reg c) where
  m1 = Eps
  x <> y = x :> y

rec_over_reg :: (Reg c ->  Reg c) -> Reg c -> Reg c
rec_over_reg f (x :> y) = f ((rec_over_reg f x) :> (rec_over_reg f y))
rec_over_reg f (x :| y) = f ((rec_over_reg f x) :| (rec_over_reg f y))
rec_over_reg f (Many x) = f (Many (rec_over_reg f x))
rec_over_reg f x = f x

local_fix_assoc :: Reg c -> Reg c
local_fix_assoc (x :> (y :> z)) = (local_fix_assoc (x :> y)) :> z
local_fix_assoc (x :| (y :| z)) = (local_fix_assoc (x :| y)) :| z
local_fix_assoc x = x

fix_assoc :: Reg c -> Reg c
fix_assoc = rec_over_reg local_fix_assoc

remove_eps :: Reg c -> Reg c
remove_eps (x :> Eps) = x
remove_eps (Eps :> x) = x
remove_eps (Many Eps) = Eps
remove_eps x = x

remove_empty :: Reg c -> Reg c
remove_empty (_ :> Empty) = Empty
remove_empty (Empty :> _) = Empty
remove_empty (Empty :| x) = x
remove_empty (x :| Empty) = x
remove_empty (Many Empty) = Eps
remove_empty x = x

remove_many :: Reg c -> Reg c
remove_many (Many (Many x)) = Many x
remove_many x = x

remove_same :: Eq c => Reg c -> Reg c
remove_same (x :| y) = if x == y then x else x :| y
remove_same x = x


shallow_simpl :: Eq c =>  Reg c -> Reg c
shallow_simpl = remove_same . remove_many . remove_eps . remove_empty

simpl :: Eq c => Reg c -> Reg c
simpl = rec_over_reg $ shallow_simpl

nullable :: Reg c -> Bool
nullable (x :> y) = nullable x && nullable y
nullable (x :| y) = nullable x || nullable y
nullable (Many x) = True
nullable (Eps) = True
nullable _ = False

empty :: Reg c -> Bool 
empty (x :> y) = empty x || empty y
empty (x :| y) = empty x && empty y
empty (Empty) = True
empty _ = False

der_h :: Eq c => c -> Reg c -> (Bool, Reg c)
der_h c (Lit x)
      | c == x = (False, Eps)
      | otherwise = (False, Empty)

der_h c (x :> y)
      | nx' = (ny', (shallow_simpl $ x' :> y) :| y')
      | otherwise = (False, x' :> y)
    where
        (nx', x'') = der_h c x
        x' = shallow_simpl x''
        (ny', y'') = der_h c y
        y' = shallow_simpl y''

der_h c (x :| y) = 
        (nx' || ny', x' :| y')
        where
            (nx', x'') = der_h c x
            x' = shallow_simpl x''
            (ny', y'') = der_h c y
            y' = shallow_simpl y''

der_h c (Many x) = 
        (True, x' :> (Many x))
        where
            (nx', x'') = der_h c x
            x' = shallow_simpl x''

der_h c (Eps) = (True, Empty)
der_h c (Empty) = (False, Empty)

der :: Eq c => c -> Reg c -> Reg c
der c r = shallow_simpl $ snd (der_h c r)

ders :: Eq c => [c] -> Reg c -> Reg c
ders c r = foldl (flip der) (simpl r) c

accepts :: Eq c => Reg c -> [c] -> Bool
accepts r w = nullable $ ders w r

mayStart :: Eq c => c -> Reg c -> Bool
mayStart c r = not $ empty $ der c r

match :: Eq c => Reg c -> [c] -> Maybe [c]
match r w =
        find (\x -> accepts r (take x w)) [(length w), (length w) - 1.. 0] >>=
        \x -> return (take x w)

isJust :: Maybe a -> Bool
isJust Nothing = False
isJust _ = True

search :: Eq c => Reg c -> [c] -> Maybe [c]
search r w = 
        find (\x -> isJust (match r (drop x w))) [0 .. (length w)] >>=
        \x -> match r (drop x w)

findall :: Eq c => Reg c -> [c] -> [[c]]
findall r [] = if nullable r then [[]] else []
findall r w = case match r w of
                  Nothing -> findall r (tail w)
                  Just c -> c : findall r (drop (max (length c) 1) w)

char :: Char -> Reg Char
char = Lit

string :: [Char] -> Reg Char
string = foldr1 (:>) . map Lit

alts :: [Char] -> Reg Char
alts = foldr1 (:|) . map Lit

letter = alts ['a'..'z'] :| alts ['A'..'Z']
digit = alts ['0'..'9']
number = digit :> Many digit
ident = letter :> Many (letter :| digit)

many1 r = r :> Many r
